<?php

/**
 * @file
 * uw_cfg_redirect.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_cfg_redirect_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer redirects'.
  $permissions['administer redirects'] = array(
    'name' => 'administer redirects',
    'roles' => array(
      'WCMS support' => 'WCMS support',
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'redirect',
  );

  return $permissions;
}
